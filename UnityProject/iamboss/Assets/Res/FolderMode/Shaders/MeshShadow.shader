﻿Shader "Shadow/MeshShadow"
{
    Properties
    {
	    _ShadowIntensity("_ShadowIntensity",Float) =1.0
		_WorldPos("WorldPos", Vector) = (1,1,1,1)
		_ShadowPlane("ShadowPlane", Vector) = (0,1, 0, 0.1)
		_ShadowProjDir("ShadowProjDir", Vector) = (0, 1.5, 0, 0)
		_ShadowFalloff ("ShadowFalloff", Float) = 1.0 
    }
    SubShader {
        Tags 
		{
            "RenderType"="Transparent"  "Queue" = "Transparent" 
		}  
        Pass
		{		
			Blend SrcAlpha  OneMinusSrcAlpha
			ZWrite Off
			Cull Back
			ColorMask RGB
			
			Stencil
			{
				Ref 0			
				Comp Equal			
				WriteMask 255		
				ReadMask 255
				//Pass IncrSat
				Pass Invert
				Fail Keep
				ZFail Keep
			}
			
			CGPROGRAM
			
			#pragma vertex vert
			#pragma fragment frag

			float4 _ShadowPlane;
			float4 _ShadowProjDir;
			float4 _WorldPos;
			half _ShadowFalloff;
			half _ShadowIntensity;
			
			struct appdata
			{
				float4 vertex : POSITION;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float3 xlv_TEXCOORD0 : TEXCOORD0;
				float3 xlv_TEXCOORD1 : TEXCOORD1;
			};

			v2f vert(appdata v)
			{
				v2f o;

				float3 lightdir = normalize(_ShadowProjDir);
				float3 worldpos = mul(unity_ObjectToWorld, v.vertex).xyz;
				// _ShadowPlane.w = p0 * n  // 平面的w分量就是p0 * n
				float distance = (_ShadowPlane.w - dot(_ShadowPlane.xyz, worldpos)) / dot(_ShadowPlane.xyz, lightdir.xyz);
				worldpos = worldpos + distance * lightdir.xyz;
				o.vertex = mul(unity_MatrixVP, float4(worldpos, 1.0));
				o.xlv_TEXCOORD0 = _WorldPos.xyz;
				o.xlv_TEXCOORD1 = worldpos;

				return o;
			}
			
			half4 frag(v2f i) : SV_Target
			{
				float3 posToPlane_2 = (i.xlv_TEXCOORD0 - i.xlv_TEXCOORD1);
				half4 color;
				color.xyz = half3(0,0, 0);
				
				// 下面两种阴影衰减公式都可以使用(当然也可以自己写衰减公式)
				//color.w = (pow((1.0 - clamp(((sqrt(dot(posToPlane_2, posToPlane_2)) * _ShadowInvLen) - _ShadowFadeParams.x), 0.0, 1.0)), _ShadowFadeParams.y) * _ShadowFadeParams.z);

				// 另外的阴影衰减公式
				color.w = 1.0 - saturate(distance(i.xlv_TEXCOORD0, i.xlv_TEXCOORD1) * _ShadowFalloff);
				color.w = color.w *_ShadowIntensity;
				return color;
			}
			
			ENDCG
		}
    }
	
    FallBack "Diffuse"
}
