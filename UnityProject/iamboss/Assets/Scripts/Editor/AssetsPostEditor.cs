﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace Qarth
{
    public class AssetsPostEditor : AssetPostprocessor
    {
        static void OnPostprocessAllAssets(
            string[] importedAssets,
            string[] deletedAssets,
            string[] movedAssets,
            string[] movedFromAssetPaths)
        {
            foreach (var path in importedAssets)
            {
                HandleSkeletonData(path);
            }
        }

        static void HandleSkeletonData(string path)
        {
            // 判断文件是不是配置文件 .csv, json的.txt (个人角色json的配置文件就是以.json为后缀名是最为合理的！)
            if (path.EndsWith(".asset") && path.Contains("SkeletonData"))
            {
                var obj = AssetDatabase.LoadAssetAtPath(path, typeof(Spine.Unity.SkeletonDataAsset));
                if (obj != null)
                {
                    (obj as Spine.Unity.SkeletonDataAsset).defaultMix = 0f;

                    AssetDatabase.Refresh();
                }

            }
        }
    }
}