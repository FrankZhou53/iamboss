﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class AnimMoveUp : MonoBehaviour
{
    private void Start()
    {
        int y = Random.Range(160, 300);
        float dur = Random.Range(3.5f, 5.0f);
        float delay = Random.Range(0.6f, 1.2f);
        transform.DOLocalMoveY(y, dur).SetDelay(delay).SetLoops(-1);
        //PlayAnim();
    }

    void PlayAnim()
    {
        int y = Random.Range(160, 300);
        float dur = Random.Range(3.5f, 5.0f);
        float delay = Random.Range(0.6f, 1.2f);
        transform.DOLocalMoveY(y, dur).SetDelay(delay).SetLoops(1).OnComplete(() => { PlayAnim(); });
    }
}
