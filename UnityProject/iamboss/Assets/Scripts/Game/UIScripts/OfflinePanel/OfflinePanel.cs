﻿using System.Collections;
using System.Collections.Generic;
using Qarth;
using UnityEngine;
using UnityEngine.UI;

namespace GameWish.Game
{
    public class OfflinePanel : AbstractAnimPanel
    {
        [SerializeField]
        private Text m_TxtReward;
        [SerializeField]
        private Button m_BtnClose;

        [SerializeField]
        private Button m_BtnConfirm;
        [SerializeField]
        private Text m_TxtCount;

        private BigInteger m_Reward;
        protected override void OnUIInit()
        {
            base.OnUIInit();
            m_BtnClose.onClick.AddListener(OnClickClose);
            m_BtnConfirm.onClick.AddListener(OnClickConfirm);
            m_TxtCount.gameObject.SetActive(false);
        }

        protected override void OnPanelOpen(params object[] args)
        {
            base.OnPanelOpen(args);

        }

        protected override void OnPanelHideComplete()
        {
            base.OnPanelHideComplete();
            CloseSelfPanel();
        }

        protected override void OnClose()
        {
            base.OnClose();
        }

        void GetReward(int rate = 1)
        {
            //            PlayerInfoMgr.AddGameToken(m_Reward * rate);
            HideSelfWithAnim();
        }

        void OnClickConfirm()
        {
            CustomExtensions.PlayAd("OfflineReward", (click) =>
            {
                GetReward(2);
            });
        }

        void OnClickClose()
        {
            GetReward();
        }
    }
}

