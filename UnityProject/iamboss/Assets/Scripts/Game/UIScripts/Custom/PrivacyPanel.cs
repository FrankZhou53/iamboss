﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Qarth;

namespace GameWish.Game 
{
    public class PrivacyPanel : AbstractPanel
    {
        [SerializeField]private Button m_BtnOk;
        [SerializeField] private PrivacyType m_PrivacyType=PrivacyType.ModooPlay;

        #region PrivacyTxt
        [SerializeField] private Text m_TxtModoo;
        [SerializeField] private Text m_TxtWebeye;
        #endregion

        protected override void OnUIInit()
        {
            base.OnUIInit();
            m_BtnOk.onClick.AddListener(CloseSelfPanel);
        }

        protected override void OnOpen()
        {
            base.OnOpen();
            switch (m_PrivacyType)
            {
                case PrivacyType.WebEye:
                    m_TxtWebeye.gameObject.SetActive(true);
                    m_TxtModoo.gameObject.SetActive(false);
                    break;
                case PrivacyType.ModooPlay:
                    m_TxtWebeye.gameObject.SetActive(false);
                    m_TxtModoo.gameObject.SetActive(true);
                    break;
                default:
                    m_TxtWebeye.gameObject.SetActive(false);
                    m_TxtModoo.gameObject.SetActive(true);
                    break;
            }
        }

        protected override void OnClose()
        {
            base.OnClose();
        }
    }

    public enum PrivacyType
    {
        /// <summary>
        /// 魔度
        /// </summary>
        ModooPlay=0,
        /// <summary>
        /// 网觉
        /// </summary>
        WebEye,
    }
}

