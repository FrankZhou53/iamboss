using UnityEngine;
using UnityEngine.Events;

namespace Qarth
{
    [System.Serializable]
    public class UnityEvent_Object : UnityEvent<GameObject>
    {
    }
}