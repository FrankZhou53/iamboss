using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Qarth;
using UnityEngine;
using UnityEngine.UI;

namespace GameWish.Game
{
    public class LoadingPanel : AbstractAnimPanel
    {
        [SerializeField]
        private Transform m_TrsRootTexts;

        private Text[] m_LoadingTexts;
        private float m_TotalDelay;
        private int m_TimerId;

        protected override void OnUIInit()
        {
            base.OnUIInit();
            m_LoadingTexts = m_TrsRootTexts.GetComponentsInChildren<Text>();
            m_TotalDelay = m_LoadingTexts.Length * 0.1f + 0.3f;
        }

        protected override void OnPanelOpen(params object[] args)
        {
            base.OnPanelOpen(args);
            RegisterEvent(EventID.OnSceneLoaded, OnSceneLoaded);

            CleanTimer();
            DOTextJump();
            m_TimerId = Timer.S.Post2Scale((count) =>
            {
                DOTextJump();
            }, m_TotalDelay, -1);
        }


        protected override void OnClose()
        {
            base.OnClose();
            for (int i = 0; i < m_LoadingTexts.Length; i++)
            {
                m_LoadingTexts[i].transform.DOKill(true);
            }
            CleanTimer();
        }

        void OnSceneLoaded(int key, params object[] args)
        {
            HideSelfWithAnim();
        }

        protected override void OnPanelHideComplete()
        {
            base.OnPanelHideComplete();
            CloseSelfPanel();
        }

        void CleanTimer()
        {
            if (m_TimerId > 0)
            {
                Timer.S.Cancel(m_TimerId);
                m_TimerId = -1;
            }
        }

        void DOTextJump()
        {
            for (int i = 0; i < m_LoadingTexts.Length; i++)
            {
                m_LoadingTexts[i].transform.DOKill(true);
                m_LoadingTexts[i].transform.DOJump(m_LoadingTexts[i].transform.position,
                    RandomHelper.Range(0.3f, 0.5f), 1, 0.4f).SetDelay(i * 0.1f);
            }
        }
    }
}

