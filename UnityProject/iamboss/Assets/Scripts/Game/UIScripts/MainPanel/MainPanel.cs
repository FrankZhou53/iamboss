using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Qarth;
using UnityEngine;
using UnityEngine.UI;

namespace GameWish.Game
{
    public class MainPanel : AbstractPanel
    {
        //top btn
        [SerializeField]
        private Button m_BtnStart;

        protected override void OnUIInit()
        {
            base.OnUIInit();
            m_BtnStart.onClick.AddListener(OnClickStart);
        }

        protected override void OnPanelOpen(params object[] args)
        {
            base.OnPanelOpen(args);
        }


        protected override void OnClose()
        {
            base.OnClose();
            EventSystem.S.Send(EventID.OnPanelClose, UIID.MainPanel);
        }

        void OnClickStart()
        {
            SceneLogicMgr.S.ChangeScene(SceneEnum.Gaming, false);
            CloseSelfPanel();
        }
    }
}

