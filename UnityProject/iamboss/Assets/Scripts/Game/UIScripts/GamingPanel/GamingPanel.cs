﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using Qarth;
using UnityEngine.UI;

namespace GameWish.Game
{
    public class GamingPanel : AbstractPanel
    {
        [SerializeField] private Text m_TxtPt;
        [SerializeField] private Text m_TxtCash;
        [SerializeField] private Text m_TxtCard;

        [SerializeField] private Button m_BtnPt;
        [SerializeField] private Button m_BtnCash;
        [SerializeField] private Button m_BtnCard;
        [SerializeField] private Button m_BtnCollects;

        [SerializeField] private Button m_BtnSetting;
        [SerializeField] private Button m_BtnRule;
        [SerializeField] private Button m_BtnRmBlock;
        [SerializeField] private Button m_BtnSupply;

        [SerializeField] private Image m_ImgCard;
        [SerializeField] private Image m_ImgCollection;

        protected override void OnUIInit()
        {
            base.OnUIInit();
            m_BtnPt.onClick.AddListener(OnClickProperty);
            m_BtnCash.onClick.AddListener(OnClickProperty);
            m_BtnCard.onClick.AddListener(OnClickProperty);
            m_BtnCollects.onClick.AddListener(OnClickCollects);

            m_BtnSetting.onClick.AddListener(OnClickSetting);
            m_BtnRule.onClick.AddListener(OnClickRule);
            m_BtnRmBlock.onClick.AddListener(OnClickRmBlock);
            m_BtnSupply.onClick.AddListener(OnClickSupply);
        }

        protected override void OnPanelOpen(params object[] args)
        {
            base.OnPanelOpen(args);
            RegisterEvent(EventID.OnPropertyAdd, OnPropertyAdd);
            RegisterEvent(EventID.OnCardGet, OnCardGet);
            RegisterEvent(EventID.OnCollectionGet, OnCollectionGet);
            OnPropertyAdd(0);
        }

        protected override void OnClose()
        {
            base.OnClose();

        }


        #region btns
        void OnClickProperty()
        {
            UIMgr.S.OpenPanel(UIID.RedeemPanel);
        }
        void OnClickCollects()
        {
            UIMgr.S.OpenPanel(UIID.CollectionPanel);
        }

        void OnClickRmBlock()
        {
            UIMgr.S.OpenPanel(UIID.RemoveBlockPanel);
        }

        void OnClickSupply()
        {
            UIMgr.S.OpenPanel(UIID.DirectHitPanel);
        }

        void OnClickSetting()
        {
            UIMgr.S.OpenPanel(UIID.SettingPanel);
        }
        void OnClickRule()
        {
            UIMgr.S.OpenPanel(UIID.RulePanel);
            //EventSystem.S.Send(EventID.OnSlotTrigger);
        }
        #endregion


        #region evt
        void OnPropertyAdd(int key, params object[] args)
        {
            m_TxtPt.text = PlayerInfoMgr.data.gameToken;
            m_TxtCash.text = string.Format("{0:N2}", PlayerInfoMgr.data.cash);
            m_TxtCard.text = PlayerInfoMgr.data.cardTicket.ToString();
        }

        void OnCardGet(int key, params object[] args)
        {
            if (args != null && args.Length > 0)
            {
                m_ImgCard.gameObject.SetActive(true);
                m_ImgCard.transform.localScale = Vector3.zero;
                CustomExtensions.ScenePosition2UIPosition(GameCamMgr.S.gameplayCamera,
                    UIMgr.S.uiRoot.uiCamera, (Vector3)args[0], m_ImgCard.transform);
                m_ImgCard.transform.DOScale(Vector3.one, 0.2f).SetEase(Ease.OutBack).OnComplete(() =>
                {
                    m_ImgCard.transform.DOScale(Vector3.one * 0.2f, 0.6f);
                    m_ImgCard.transform.DOMove(m_BtnCard.transform.position, 0.6f).SetEase(Ease.InSine).OnComplete(() =>
                    {
                        m_ImgCard.gameObject.SetActive(false);
                    });
                });
            }
        }

        void OnCollectionGet(int key, params object[] args)
        {
            if (args != null && args.Length > 0)
            {
                m_ImgCollection.gameObject.SetActive(true);
                m_ImgCollection.transform.localScale = Vector3.zero;
                CustomExtensions.ScenePosition2UIPosition(GameCamMgr.S.gameplayCamera,
                    UIMgr.S.uiRoot.uiCamera, (Vector3)args[0], m_ImgCollection.transform);
                m_ImgCollection.transform.DOScale(Vector3.one, 0.2f).SetEase(Ease.OutBack).OnComplete(() =>
                {
                    m_ImgCollection.transform.DOScale(Vector3.one * 0.2f, 0.6f);
                    m_ImgCollection.transform.DOMove(m_BtnCollects.transform.position, 0.6f).SetEase(Ease.InSine).OnComplete(() =>
                    {
                        m_ImgCollection.gameObject.SetActive(false);
                    });
                });
            }
        }
        #endregion
    }
}

