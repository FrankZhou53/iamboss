using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using DG.Tweening;

namespace GameWish.Game
{
    public class PlayerRoleState_Wander : PlayerRoleState
    {
        private Vector3 m_OriginPos;
        private Vector3 m_OriginScale;
        private float m_Timer;

        public PlayerRoleState_Wander(PlayerRoleFSMCompo owner, RoleStateEnum stateEnum) : base(owner, stateEnum)
        {
        }

        public override void Enter(EntityBase mgr)
        {
            base.Enter(mgr);
            m_Mgr.PlayBodyAnim("idle", true);
            m_Timer = RandomHelper.Range(0.5f, 1);
        }

        public override void Execute(EntityBase mgr, float dt)
        {
            base.Execute(mgr, dt);

            if (m_Timer > 0 && m_Mgr.baseData.trsModel != null)
            {
                m_Timer -= dt;
                if (m_Timer < 0)
                {
                    float offset = RandomHelper.Range(-2f, -1f);
                    var tarPos = m_Mgr.LegalPosInGround(new Vector3(0, offset, 0));
                    float dur = Vector3.Distance(tarPos, m_Mgr.baseData.trsModel.localPosition) / 1f;
                    m_Mgr.baseData.trsModel.DOLocalMove(tarPos, dur)
                        .OnComplete(OnWanderEnd);
                    m_Mgr.CheckPlayBodyAnim("move", true);
                }
            }
        }
        public override void Exit(EntityBase mgr)
        {
            base.Exit(mgr);
            m_Mgr.baseData.trsModel.DOKill();
        }

        void OnWanderEnd()
        {

            {
                m_Mgr.PlayBodyAnim("idle", true);
                m_Timer = RandomHelper.Range(3f, 8f);
            }
        }
    }
}