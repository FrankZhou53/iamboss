using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{
    public class PlayerRoleState_Idle : PlayerRoleState
    {
        public PlayerRoleState_Idle(PlayerRoleFSMCompo owner, RoleStateEnum stateEnum) : base(owner, stateEnum)
        {
        }

        public override void Enter(EntityBase mgr)
        {
            base.Enter(mgr);
            m_Mgr.PlayBodyAnim("idle", true);
            //EventSystem.S.Send(EventID.OnRestartPlayerRoleMove, m_Mgr.EntityID);
        }

        public override void Execute(EntityBase mgr, float dt)
        {
            base.Execute(mgr, dt);
        }
        public override void Exit(EntityBase mgr)
        {
            base.Exit(mgr);
        }
    }
}