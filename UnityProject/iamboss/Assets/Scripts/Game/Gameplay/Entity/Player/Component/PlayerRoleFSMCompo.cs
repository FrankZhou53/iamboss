using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{
    public class PlayerRoleFSMCompo : EntityFSMCompo
    {
        private PlayerRoleBase m_PlayerRoleOwner;
        private PlayerRoleBase PlayerRoleOwner
        {
            get
            {
                if (m_PlayerRoleOwner == null)
                    m_PlayerRoleOwner = m_Owner as PlayerRoleBase;
                return m_PlayerRoleOwner;
            }
        }

        public RoleStateEnum PlayerRoleState
        {
            get
            {
                return (fsmMachine.currentState as PlayerRoleState).CurrentStateEnum;
            }
        }

        public RoleStateEnum prevPlayerRoleState
        {
            get
            {
                return fsmMachine.previousState == null ?
                    RoleStateEnum.Idle : (fsmMachine.previousState as PlayerRoleState).CurrentStateEnum;
            }
        }

        public override void InitComponent(EntityBase owner)
        {
            base.InitComponent(owner);
            m_FSMEntity.stateFactory.RegisterState(RoleStateEnum.Idle,
                new PlayerRoleState_Idle(this, RoleStateEnum.Idle));
            m_FSMEntity.stateFactory.RegisterState(RoleStateEnum.Wander,
                new PlayerRoleState_Wander(this, RoleStateEnum.Wander));

            fsmMachine.SetCurrentStateByID(RoleStateEnum.Idle);
            // EventSystem.S.Register(EventID.OnRoleSettled, OnPlayerRoleSettled);
            // EventSystem.S.Register(EventID.OnRolePicked, OnPlayerRolePicked);

        }

        public override void Start()
        {
            base.Start();
        }
        public override void Tick(float deltaTime)
        {
            base.Tick(deltaTime);
        }

        public override void OnDestory()
        {
            base.OnDestory();
            // EventSystem.S.UnRegister(EventID.OnRoleSettled, OnPlayerRoleSettled);
            // EventSystem.S.UnRegister(EventID.OnRolePicked, OnPlayerRolePicked);

            m_PlayerRoleOwner = null;
        }

        public void SetState(RoleStateEnum state, bool force = false)
        {
            if (fsmMachine.currentState != null)
            {
                var curState = (fsmMachine.currentState as PlayerRoleState).CurrentStateEnum;
                if (!force && curState == state)
                {
                    Log.i("same state with no force set:" + state);
                    return;
                }
            }

            fsmMachine.SetCurrentStateByID(state);
        }



        #region fsm_evt
        public void OnPlayerRoleSettled(int key, params object[] args)
        {
            if (GameExtensions.IsSameEntity(m_Owner.EntityID, args))
            {
                SetState(RoleStateEnum.Wander);
            }
        }

        public void OnPlayerRolePicked(int key, params object[] args)
        {
            if (GameExtensions.IsSameEntity(m_Owner.EntityID, args))
            {
                SetState(RoleStateEnum.Idle);
            }
        }

        #endregion
    }
}