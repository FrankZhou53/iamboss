using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{
    public class PlayerRoleRenderCompo : RoleRenderCompo
    {
        private float m_StartScale = 1f;
        private float m_PickScale = 1.15f;
        private float m_SettleScale = 1f;

        public PlayerRoleRenderCompo(PlayerRoleData data, ResLoader loader) : base(loader)
        {
            m_Data = data;
        }

        public override void InitComponent(EntityBase owner)
        {
            base.InitComponent(owner);
            LoadModel();
            //EventSystem.S.Register(EventID.OnRestartPlayerRoleMove, OnRestartPlayerRoleMove);
        }

        public override void Start()
        {
            base.Start();
            m_ObjBody.SetAllLayer(LayerMask.NameToLayer("PlayerRole"));
            m_ObjBody.tag = "PlayerRole";
        }

        public override void Tick(float deltaTime)
        {
            base.Tick(deltaTime);
        }

        private void LoadModel()
        {
            var data = m_Data as PlayerRoleData;
            var prefab = m_Loader.LoadSync(data.GetBasePrefab());
            var obj = GameObject.Instantiate(prefab) as GameObject;
            m_ObjBody = new GameObject("PlayerRole_" + m_Owner.EntityID);
            //m_ObjBody.transform.parent = GameplayMgr.S.trsRootEntity;
            obj.name = "body";
            obj.transform.parent = m_ObjBody.transform;
            //obj.transform.ResetTrans(data.conf.defaultScale);
            obj.transform.localEulerAngles = new Vector3(0, 0, 0);

            m_ObjBody.transform.localPosition = GetStartPos();
            m_ObjBody.transform.localEulerAngles = new Vector3(0, 0, 0);
            m_ObjBody.transform.localScale = Vector3.one * m_StartScale;

            data.objModel = m_ObjBody;
            var pBox = m_ObjBody.AddMissingComponent<BoxCollider>();
            pBox.isTrigger = true;
            var bounds = data.animBody.GetComponent<MeshRenderer>().bounds;
            pBox.size = new Vector3(bounds.size.x, bounds.size.y + 0.5f, 1.8f);
            pBox.center = bounds.center - m_ObjBody.transform.position;
            pBox.center = new Vector3(pBox.center.x, pBox.center.y, 0.5f);
        }

        public override void OnDestory()
        {
            if (m_ObjBody != null)
            {
                GameObject.Destroy(m_ObjBody);
            }
            m_ObjBody = null;
            m_Loader = null;
            m_Data = null;
            //EventSystem.S.UnRegister(EventID.OnRestartPlayerRoleMove, OnRestartPlayerRoleMove);
        }

        private Vector3 GetStartPos()
        {
            return new Vector3(0, 6, 0);
        }
        void OnRestartPlayerRoleMove(int key, params object[] para)
        {
            if (GameExtensions.IsSameEntity(m_Owner.EntityID, para))
            {
                m_ObjBody.transform.localPosition = GetStartPos();
                //(m_Owner as PlayerRoleBase).PlayerRoleMove();
            }
        }
    }
}