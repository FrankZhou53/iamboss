using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Qarth;
using UnityEngine;

namespace GameWish.Game
{
    public class PlayerRoleBase : RoleBase
    {
        public PlayerRoleData data
        {
            get { return m_Data as PlayerRoleData; }
        }

        public PlayerRoleBase() : base()
        {
            m_Data = new PlayerRoleData(this);
            m_Data.InitFromConf();

            AddComponent(new PlayerRoleRenderCompo(data, GameplayMgr.S.MainLoader));
            AddComponent(new PlayerRoleFSMCompo());
        }

        protected override void Start()
        {
            base.Start();
        }

        protected override void OnDesotrySelf()
        {
            base.OnDesotrySelf();
            m_Data = null;
        }
    }
}

