﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using DG.Tweening;

namespace GameWish.Game
{
    public class CastleBase : MonoBehaviour
    {
        [SerializeField]
        private List<GameObject> m_LstEnemy = new List<GameObject>();
        // Start is called before the first frame update
        private int m_Hp = 10;
        void Start()
        {
            m_Hp = 10;
        }

        // Update is called once per frame
        void Update()
        {

        }
        private void OnTriggerEnter(Collider collision)
        {
            if (collision.transform.tag == "PlayerJump")
            {
                foreach (var item in m_LstEnemy)
                {
                    BattleMgr.S.CreateBloodC(item.transform.position);
                    Destroy(item);
                }
                m_LstEnemy.Clear();
                m_Hp -= 4;
                if (!IsDestory())
                {
                    BattleMgr.S.CreateBuildingDestory(this.transform.position + new Vector3(1f, 0, 1f), 2);
                }
            }
            if (collision.transform.tag == "PlayerHand")
            {
                foreach (var item in m_LstEnemy)
                {
                    BattleMgr.S.CreateBloodC(item.transform.position + new Vector3(0, 0.3f, 0));
                    Destroy(item);
                }
                m_LstEnemy.Clear();
                m_Hp -= 1;
                if (!IsDestory())
                {
                    BattleMgr.S.CreateBuildingDestory(this.transform.position + new Vector3(1f, 0, 1f), 2);
                }
            }
        }
        private void OnTriggerExit(Collider collision)
        {

        }
        bool IsDestory()
        {
            if (m_Hp <= 0)
            {
                BattleMgr.S.CreateBuildingDestory(this.transform.position + new Vector3(1f, 0, 1f), 2);
                transform.DOBlendableMoveBy(new Vector3(0, -20, 0), 0.8f);
                CustomExtensions.CallWithDelay(this, () =>
                {
                    Destroy(this.gameObject);
                }, 0.8f);
                return true;
            }
            return false;
        }
    }
}
