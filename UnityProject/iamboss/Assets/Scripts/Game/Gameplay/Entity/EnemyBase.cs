﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{
    public class EnemyBase : MonoBehaviour
    {
        // Start is called before the first frame update
        [SerializeField]
        private Animator m_Animator;
        public float m_speed = 50f;
        private bool isFly = false;
        private bool isRun = false;
        void Start()
        {
            isFly = false;
            GetComponent<Rigidbody>().freezeRotation = true;
        }

        // Update is called once per frame
        void Update()
        {
            if (isRun)
            {
                TurnToPlayer();
            }
        }
        void OnCollisionEnter(Collision collision)
        {
            //进入碰撞器执行的代码
            if (isFly)
            {
                if (collision.transform.tag == "MapGround" || collision.transform.tag == "Building")
                {
                    EnemyDied();
                }
            }
            else
            {
                if (collision.transform.tag == "Building")
                {

                    GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
                    m_Animator.SetInteger("Animation_Int", 0);
                }
            }
        }
        void OnCollisionExit(Collision collision)
        {
            //进入碰撞器执行的代码
        }
        private void OnTriggerEnter(Collider collision)
        {
            if (!isFly)
            {
                if (collision.transform.tag == "PlayerJump")
                {
                    EnemyDied();
                }
                if (collision.transform.tag == "PlayerFoot")
                {
                    if (RandomHelper.Range(0, 100) < 50)
                    {
                        EnemyFly(collision.transform.position, 7000);
                    }
                    else
                    {
                        EnemyDied();
                    }
                }
                if (collision.transform.tag == "PlayerHand")
                {
                    EnemyFly(collision.transform.position);
                }
                if (collision.transform.tag == "PlayerRunRange")
                {
                    if (m_Animator != null)
                    {
                        m_Animator.SetInteger("Animation_Int", 1);
                        CustomExtensions.CallWithDelay(this, () =>
                        {
                            isRun = true;
                        }, 0.9f);

                    }
                }
                if (collision.transform.tag == "PlayerAtkRange")
                {
                    if (m_Animator != null)
                    {
                        m_Animator.SetInteger("Animation_Int", 2);
                        TurnToPlayer();
                    }
                }
            }
        }
        private void OnTriggerExit(Collider collision)
        {
            if (!isFly)
            {
                if (collision.transform.tag == "PlayerAtkRange")
                {
                    if (m_Animator != null)
                    {
                        m_Animator.SetInteger("Animation_Int", 1);
                        // if (RandomHelper.Range(0, 100) < 50)
                        // {
                        //     TurnOverToPlayer();
                        // }
                        // else
                        // {
                        //     TurnOverOutPlayer();
                        // }
                    }
                }
            }
        }
        void EnemyDied()
        {
            if (m_Animator != null)
                m_Animator.SetInteger("Animation_Int", 0);
            isRun = false;
            BattleMgr.S.CreateBloodB(this.transform.localPosition);
            Destroy(this.gameObject);
        }
        void EnemyFly(Vector3 collisionPos, int force = 20000)
        {
            if (m_Animator != null)
                m_Animator.SetInteger("Animation_Int", 0);
            isRun = false;
            isFly = true;
            GetComponent<Rigidbody>().freezeRotation = false;
            float ForceN = force / (this.transform.position - collisionPos).magnitude;
            GetComponent<Rigidbody>().AddForce((this.transform.position - collisionPos).normalized * ForceN);
        }
        void TurnToPlayer()
        {
            float sinValue2 = (transform.position.x - BattleMgr.S.player.transform.position.x) / Mathf.Sqrt(Mathf.Pow((transform.position.x - BattleMgr.S.player.transform.position.x), 2) + Mathf.Pow((transform.position.z - BattleMgr.S.player.transform.position.z), 2));//正弦值 = 0.5。
            float sinRadianValue2 = Mathf.Asin(sinValue2);//求弧度值
            float sinAngleValue2 = sinRadianValue2 / Mathf.PI * 180;//根据弧度值，来求角度值。
            if (transform.position.z - BattleMgr.S.player.transform.position.z > 0)
            {
                transform.localRotation = Quaternion.Euler(new Vector3(0, 180 + sinAngleValue2, 0));
            }
            else
            {
                transform.localRotation = Quaternion.Euler(new Vector3(0, -sinAngleValue2, 0));
            }
        }
        void TurnOutPlayer()
        {
            float sinValue2 = (transform.position.x - BattleMgr.S.player.transform.position.x) / Mathf.Sqrt(Mathf.Pow((transform.position.x - BattleMgr.S.player.transform.position.x), 2) + Mathf.Pow((transform.position.z - BattleMgr.S.player.transform.position.z), 2));//正弦值 = 0.5。
            float sinRadianValue2 = Mathf.Asin(sinValue2);//求弧度值
            float sinAngleValue2 = sinRadianValue2 / Mathf.PI * 180;//根据弧度值，来求角度值。
            transform.localRotation = Quaternion.Euler(new Vector3(0, sinAngleValue2, 0));
        }
        void TurnOverToPlayer()
        {
            float sinValue2 = (transform.position.x - BattleMgr.S.player.transform.position.x) / Mathf.Sqrt(Mathf.Pow((transform.position.x - BattleMgr.S.player.transform.position.x), 2) + Mathf.Pow((transform.position.z - BattleMgr.S.player.transform.position.z), 2));//正弦值 = 0.5。
            float sinRadianValue2 = Mathf.Asin(sinValue2);//求弧度值
            float sinAngleValue2 = sinRadianValue2 / Mathf.PI * 180;//根据弧度值，来求角度值。
            transform.localRotation = Quaternion.Euler(new Vector3(0, -sinAngleValue2, 0));
        }
        void TurnOverOutPlayer()
        {
            float sinValue2 = (transform.position.x - BattleMgr.S.player.transform.position.x) / Mathf.Sqrt(Mathf.Pow((transform.position.x - BattleMgr.S.player.transform.position.x), 2) + Mathf.Pow((transform.position.z - BattleMgr.S.player.transform.position.z), 2));//正弦值 = 0.5。
            float sinRadianValue2 = Mathf.Asin(sinValue2);//求弧度值
            float sinAngleValue2 = sinRadianValue2 / Mathf.PI * 180;//根据弧度值，来求角度值。
            transform.localRotation = Quaternion.Euler(new Vector3(0, 180 - sinAngleValue2, 0));
        }
    }
}
