﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using DG.Tweening;

namespace GameWish.Game
{
    public class PlayerBase : MonoBehaviour
    {
        // Start is called before the first frame update
        [SerializeField]
        private GameObject m_ObjSkillJumpRange;
        [SerializeField]
        private GameObject m_ObjAttack1Range;
        [SerializeField]
        private GameObject m_ObjAttack2Range;
        [SerializeField]
        private List<GameObject> m_LstEffect;
        public float m_speed = 50f;
        private bool isJump = false;
        private bool isCanMove = true;
        private int killnum = 0;

        void Start()
        {
            //GetComponent<Animation>().clip = GetComponent<Animation>().GetClip("Walk Forward In Place");
            // GetComponent<Animation>().clip = GetComponent<Animation>().GetClip("mixamo");
            // GetComponent<Animation>().Play();
            m_ObjSkillJumpRange.SetActive(false);
            m_ObjAttack1Range.SetActive(false);
            m_ObjAttack2Range.SetActive(false);
            isJump = false;
            isCanMove = true;
            foreach (var item in m_LstEffect)
            {
                item.SetActive(false);
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (isCanMove)
            {
                if (Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) //前
                {
                    this.transform.Translate(Vector3.forward * m_speed * Time.deltaTime);
                    MoveWalk();
                }
                if (Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)) //后
                {
                    this.transform.Translate(Vector3.forward * -m_speed * Time.deltaTime);
                    MoveWalk();
                }
            }
            // if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow)) //左
            // {
            //     this.transform.Translate(Vector3.right * -m_speed * Time.deltaTime);
            //     MoveRun();
            // }
            // if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)) //右
            // {
            //     this.transform.Translate(Vector3.right * m_speed * Time.deltaTime);
            //     MoveRun();
            // }
            if (Input.GetKeyDown(KeyCode.Space)) //跳
            {
                Jump();
            }
            if (Input.GetKeyUp(KeyCode.W) | Input.GetKeyUp(KeyCode.UpArrow)
                || Input.GetKeyUp(KeyCode.S) | Input.GetKeyUp(KeyCode.DownArrow)
                || Input.GetKeyUp(KeyCode.A) | Input.GetKeyUp(KeyCode.LeftArrow)
                || Input.GetKeyUp(KeyCode.D) | Input.GetKeyUp(KeyCode.RightArrow)
            )
            {
                Idle();
            }
            if (Input.GetKeyDown(KeyCode.J))
            {
                Attack1();
            }
            if (Input.GetKeyDown(KeyCode.K))
            {
                Attack2();
            }
            if (Input.GetKeyDown(KeyCode.G))
            {
                KillEnemy();
            }
        }
        void OnCollisionEnter(Collision collision)
        {
            //进入碰撞器执行的代码
            if (collision.transform.tag == "Building")
            {
                isCanMove = false;
            }
        }
        public void MoveRun()
        {
            if (!isJump)
                GetComponent<Animation>().Play("Run1");
        }
        public void MoveWalk()
        {
            if (!isJump)
                GetComponent<Animation>().Play("Walk");
        }
        public void Idle()
        {
            isCanMove = true;
            if (!isJump)
                GetComponent<Animation>().Play("Idle");
        }
        public void Jump()
        {
            if (!isJump)
            {
                isJump = true;
                GetComponent<Animation>().Play("Jump");
                CustomExtensions.CallWithDelay(this, () =>
                {
                    BattleMgr.S.CreateEarthQuake(this.transform.localPosition);
                    m_ObjSkillJumpRange.SetActive(true);
                }, 1f);
                CustomExtensions.CallWithDelay(this, () =>
                {
                    m_ObjSkillJumpRange.SetActive(false);
                }, 1.1f);
                CustomExtensions.CallWithDelay(this, () =>
                {
                    isJump = false;
                    m_ObjSkillJumpRange.SetActive(false);
                    Idle();
                }, 1.8f);
            }
        }
        public void Attack1()
        {
            GetComponent<Animation>().Play("attack1");
            CustomExtensions.CallWithDelay(this, () =>
            {
                m_ObjAttack1Range.SetActive(true);
            }, 0.9f);
            CustomExtensions.CallWithDelay(this, () =>
            {
                m_ObjAttack1Range.SetActive(false);
            }, 1.1f);
        }
        public void Attack2()
        {
            GetComponent<Animation>().Play("attack2");
            CustomExtensions.CallWithDelay(this, () =>
            {
                m_ObjAttack2Range.SetActive(true);
            }, 0.2f);
            CustomExtensions.CallWithDelay(this, () =>
            {
                m_ObjAttack2Range.SetActive(false);
            }, 0.4f);
        }
        public void KillEnemy()
        {
            // Log.e(killnum);
            // if (killnum++ > 200)
            // {
            foreach (var item in m_LstEffect)
            {
                item.SetActive(true);
            }
            transform.DOScale(7.0f, 3f);
            //}
        }
    }
}
