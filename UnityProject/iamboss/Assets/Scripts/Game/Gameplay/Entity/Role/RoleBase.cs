using System.Collections;
using System.Collections.Generic;
using Qarth;
using System;
using UnityEngine;
using DG.Tweening;

namespace GameWish.Game
{
    public class RoleBase : EntityBase
    {
        protected RoleData m_Data;
        public RoleData baseData
        {
            get { return m_Data; }
        }

        public RoleBase() : base()
        {

        }

        protected override void Start()
        {
            base.Start();
        }

        public override void Tick(float deltaTime)
        {
            base.Tick(deltaTime);
        }

        protected override void OnDesotrySelf()
        {
            base.OnDesotrySelf();
        }

        #region battle
        // 开启战斗AI
        public virtual void DoBattleRound()
        { }

        public virtual List<RoleBase> PickTargets()
        {
            return new List<RoleBase>();
        }

        public virtual List<RoleBase> PickAllTargets()
        {
            return new List<RoleBase>();
        }

        #endregion


        // 播动画
        public Spine.TrackEntry PlayBodyAnim(string animName, bool loop = false, int layer = 0, System.Action callback = null)
        {
            if (m_Data.animBody != null)
            {
                return m_Data.animBody.AnimationState.SetAnimation(0, animName, loop);
            }
            return null;
        }

        public Spine.TrackEntry CheckPlayBodyAnim(string animName, bool loop = false, int layer = 0, System.Action callback = null)
        {
            if (m_Data.animBody != null)
            {
                Spine.Animation animation = m_Data.animBody.skeleton.Data.FindAnimation(animName);
                if (animation != null)
                    return m_Data.animBody.AnimationState.SetAnimation(0, animName, loop);
                else
                    return m_Data.animBody.AnimationState.SetAnimation(0, "idle", loop);
            }
            return null;
        }

        public Spine.TrackEntry PlayBodyAnims(string[] animName, bool lastLoop = true, int layer = 0)
        {
            if (m_Data.animBody != null && animName != null)
            {
                if (animName.Length == 1)
                {
                    return PlayBodyAnim(animName[0], lastLoop, layer);
                }
                else if (animName.Length > 1)
                {
                    m_Data.animBody.AnimationState.SetAnimation(layer, animName[0], false);
                    for (int i = 1; i < animName.Length - 1; i++)
                    {
                        m_Data.animBody.AnimationState.AddAnimation(layer, animName[i], false, 0);
                    }
                    return m_Data.animBody.AnimationState.AddAnimation(layer, animName[animName.Length - 1], lastLoop, 0);
                }
            }
            return null;
        }

        public virtual Vector3 LegalPosInGround(Vector3 pos)
        {
            return pos;
        }
    }
}

