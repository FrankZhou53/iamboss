using System.Collections;
using System.Collections.Generic;
using Qarth;
using UnityEngine;
using Spine.Unity;

namespace GameWish.Game
{
    public class RoleData : EntityData
    {
        public RoleData(RoleBase e) : base(e)
        {
        }

        protected GameObject m_ObjModel;
        public GameObject objModel
        {
            set { m_ObjModel = value; }
            get { return m_ObjModel; }
        }
        public Transform trsModel
        {
            get { return m_ObjModel == null ? null : m_ObjModel.transform; }
        }

        protected SkeletonAnimation m_Anim;
        public SkeletonAnimation animBody
        {
            get
            {
                if (m_Anim == null && m_ObjModel != null)
                {
                    m_Anim = m_ObjModel.GetComponentInChildren<SkeletonAnimation>();
                }
                return m_Anim;
            }
        }


        public virtual void InitFromConf()
        {
        }


        public virtual string GetBasePrefab()
        {
            return null;
        }

        public virtual string GetSkin()
        {
            return null;
        }

        public virtual Vector3 GetProductPosition()
        {
            return trsModel.position;
        }

    }
}



