using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{
    public enum RoleStateEnum
    {
        Idle,
        Wander,
    }

    public class RoleFSMCompo : EntityFSMCompo
    {
        private RoleBase m_RoleOwner;
        private RoleBase roleOwner
        {
            get
            {
                if (m_RoleOwner == null)
                    m_RoleOwner = m_Owner as RoleBase;
                return m_RoleOwner;
            }
        }

        public RoleStateEnum roleState
        {
            get
            {
                return (fsmMachine.currentState as RoleState).CurrentStateEnum;
            }
        }

        public RoleStateEnum prevRoleState
        {
            get
            {
                return fsmMachine.previousState == null ?
                    RoleStateEnum.Idle : (fsmMachine.previousState as RoleState).CurrentStateEnum;
            }
        }

        public override void InitComponent(EntityBase owner)
        {
            base.InitComponent(owner);
            m_FSMEntity.stateFactory.RegisterState(RoleStateEnum.Idle,
                new RoleState_Idle(this, RoleStateEnum.Idle));
            m_FSMEntity.stateFactory.RegisterState(RoleStateEnum.Wander,
                new RoleState_Wander(this, RoleStateEnum.Wander));

            fsmMachine.SetCurrentStateByID(RoleStateEnum.Idle);


        }

        public override void Start()
        {
            base.Start();
        }
        public override void Tick(float deltaTime)
        {
            base.Tick(deltaTime);
        }

        public override void OnDestory()
        {
            base.OnDestory();

            m_RoleOwner = null;
        }

        public void SetState(RoleStateEnum state, bool force = false)
        {
            if (fsmMachine.currentState != null)
            {
                var curState = (fsmMachine.currentState as RoleState).CurrentStateEnum;
                if (!force && curState == state)
                {
                    Log.i("same state with no force set:" + state);
                    return;
                }
            }

            fsmMachine.SetCurrentStateByID(state);
        }



        #region fsm_evt
        public void OnGuideEnter(int key, params object[] args)
        {
            SetState(RoleStateEnum.Idle);
        }

        #endregion
    }
}