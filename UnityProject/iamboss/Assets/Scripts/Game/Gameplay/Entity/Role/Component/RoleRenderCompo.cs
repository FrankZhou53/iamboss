using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{
    public class RoleRenderCompo : EntityRenderCompo
    {
        protected GameObject m_ObjBody;
        protected RoleData m_Data;

        public RoleRenderCompo(ResLoader loader) : base(loader)
        {

        }

        public override void InitComponent(EntityBase owner)
        {
            base.InitComponent(owner);
        }

        public override void Start()
        {
            base.Start();
        }
    }
}