using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{
    public class RoleState : EntityState<EntityBase>
    {
        protected RoleStateEnum m_StateEnum;
        public RoleStateEnum CurrentStateEnum
        { get { return m_StateEnum; } }
        protected RoleBase m_Mgr;
        public RoleFSMCompo owner
        {
            get { return m_Owner as RoleFSMCompo; }
        }

        public RoleState(RoleFSMCompo owner, RoleStateEnum stateEnum) : base(owner)
        {
            m_StateEnum = stateEnum;
        }

        public override void Enter(EntityBase mgr)
        {
            base.Enter(mgr);
            InitState(mgr);
            //Log.e(m_StateEnum + " " + m_Mgr.EntityID);
        }

        public override void Exit(EntityBase mgr)
        {
            base.Exit(mgr);
        }

        public override void Execute(EntityBase mgr, float dt)
        {
            base.Execute(mgr, dt);
        }

        void InitState(EntityBase mgr)
        {
            if (m_Mgr == null)
            {
                m_Mgr = mgr as RoleBase;
            }
        }
    }

}