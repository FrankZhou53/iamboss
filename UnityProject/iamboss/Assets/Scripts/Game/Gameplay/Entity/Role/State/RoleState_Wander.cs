using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using DG.Tweening;

namespace GameWish.Game
{
    public class RoleState_Wander : RoleState
    {
        private Vector3 m_OriginPos;
        private Vector3 m_OriginScale;
        private float m_Timer;
        private float m_TalkTimer;

        public RoleState_Wander(RoleFSMCompo owner, RoleStateEnum stateEnum) : base(owner, stateEnum)
        {
        }

        public override void Enter(EntityBase mgr)
        {
            base.Enter(mgr);
            m_Mgr.PlayBodyAnim("idle", true);
            m_Timer = RandomHelper.Range(0.5f, 1);
            m_TalkTimer = RandomHelper.Range(4.5f, 20);
        }

        public override void Execute(EntityBase mgr, float dt)
        {
            base.Execute(mgr, dt);

            if (m_Timer > 0 && m_Mgr.baseData.trsModel != null)
            {
                m_Timer -= dt;
                if (m_Timer < 0)
                {
                    var tarPos = m_Mgr.LegalPosInGround(m_Mgr.baseData.trsModel.position +
                        RandomHelper.Range(-4f, 4f) * Vector3.right +
                        RandomHelper.Range(-6f, 6f) * Vector3.forward);
                    float dur = Vector3.Distance(tarPos, m_Mgr.baseData.trsModel.localPosition) / 2.0f;
                    m_Mgr.baseData.trsModel.DOLocalMove(tarPos, dur)
                        .OnComplete(OnWanderEnd);
                    m_Mgr.baseData.animBody.Skeleton.FlipX = tarPos.x > m_Mgr.baseData.trsModel.localPosition.x;
                    m_Mgr.CheckPlayBodyAnim("move", true);
                }
            }
        }
        public override void Exit(EntityBase mgr)
        {
            base.Exit(mgr);
            m_Mgr.baseData.trsModel.DOKill();
        }

        void OnWanderEnd()
        {
            m_Mgr.PlayBodyAnim("idle", true);
            m_Timer = RandomHelper.Range(1f, 5f);
            // m_Mgr.PlayBodyAnims(new string[] { "shit", "idle" });
            // GameExtensions.CallWithDelay(() =>
            // {
            //     if (m_Mgr.baseData != null)
            //     {
            //         DropMgr.S.CreateCoins(m_Mgr.baseData.GetProductPosition(), 1, 1, m_Mgr.baseData.animBody.Skeleton.FlipX);
            //     }
            // }, 0.65f);
        }
    }
}