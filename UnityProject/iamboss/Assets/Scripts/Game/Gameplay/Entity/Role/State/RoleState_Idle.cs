using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{
    public class RoleState_Idle : RoleState
    {
        public RoleState_Idle(RoleFSMCompo owner, RoleStateEnum stateEnum) : base(owner, stateEnum)
        {
        }

        public override void Enter(EntityBase mgr)
        {
            base.Enter(mgr);
            m_Mgr.PlayBodyAnim("idle", true);
        }

        public override void Execute(EntityBase mgr, float dt)
        {
            base.Execute(mgr, dt);
        }
        public override void Exit(EntityBase mgr)
        {
            base.Exit(mgr);
        }
    }
}