﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{
    public class CannotBase : MonoBehaviour
    {
        [SerializeField]
        private Transform m_TrsCannon;
        private bool isAttack = false;
        // Start is called before the first frame update
        private float attackTime = 0;
        void Start()
        {
            isAttack = false;
            TurnToPlayer();
        }

        // Update is called once per frame
        void Update()
        {
            TurnToPlayer();
            if (isAttack)
            {
                attackTime += Time.deltaTime;
                if (attackTime > 2f)
                {
                    attackTime = 0;
                    BattleMgr.S.CreateCannonShoot(this.transform.position + new Vector3(0, 8f, 0));
                }
            }
        }
        private void OnTriggerEnter(Collider collision)
        {
            if (collision.transform.tag == "PlayerAtkRange")
            {
                isAttack = true;
            }
        }
        private void OnTriggerExit(Collider collision)
        {
            if (collision.transform.tag == "PlayerAtkRange")
            {
                isAttack = false;
            }
        }
        void TurnToPlayer()
        {
            var oraginRotation = transform.localRotation;
            float sinValue2 = (transform.position.x - BattleMgr.S.player.transform.position.x) / Mathf.Sqrt(Mathf.Pow((transform.position.x - BattleMgr.S.player.transform.position.x), 2) + Mathf.Pow((transform.position.z - BattleMgr.S.player.transform.position.z), 2));//正弦值 = 0.5。
            float sinRadianValue2 = Mathf.Asin(sinValue2);//求弧度值
            float sinAngleValue2 = sinRadianValue2 / Mathf.PI * 180;//根据弧度值，来求角度值。
            if (transform.position.z - BattleMgr.S.player.transform.position.z > 0)
            {
                m_TrsCannon.transform.localRotation = Quaternion.Euler(new Vector3(0, sinAngleValue2, 0));
            }
            else
            {
                m_TrsCannon.transform.localRotation = Quaternion.Euler(new Vector3(0, 180 - sinAngleValue2, 0));
            }
        }
    }
}
