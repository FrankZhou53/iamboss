﻿using System.Security.AccessControl;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using DG.Tweening;

namespace GameWish.Game
{
    public class BulletBase : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {
            transform.DOMove(BattleMgr.S.player.transform.position, 1f).SetEase(Ease.Linear);
            CustomExtensions.CallWithDelay(this, () =>
            {
                BattleMgr.S.CreateBulletDestory(this.transform.position);
                Destroy(this.gameObject);
            }, 1f);
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
