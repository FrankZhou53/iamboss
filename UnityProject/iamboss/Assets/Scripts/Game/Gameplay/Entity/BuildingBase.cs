﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using DG.Tweening;

namespace GameWish.Game
{
    public class BuildingBase : MonoBehaviour
    {
        // Start is called before the first frame update
        public int type = 0;
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        void FixedUpdate()
        {
            this.GetComponent<Rigidbody>().velocity = Vector3.zero;
        }
        private void OnTriggerEnter(Collider collision)
        {
            if (collision.transform.tag == "PlayerJump")
            {
                BuildingDestory();
            }
            if (collision.transform.tag == "PlayerHand")
            {
                BuildingDestory();
            }
        }
        void BuildingDestory()
        {
            BattleMgr.S.CreateBuildingDestory(this.transform.position + new Vector3(1f, 0, 1f), type);
            transform.DOBlendableMoveBy(new Vector3(0, -5, 0), 0.5f);
            CustomExtensions.CallWithDelay(this, () =>
            {
                Destroy(this.gameObject);
            }, 0.5f);
        }
    }
}
