﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
namespace GameWish.Game
{
    public class BezierUtils
    {
        public static Vector3 CalculBZOrderPoint(float t, List<Vector3> pos)
        {
            if (pos.Count == 2)
            {
                return CalculBZOrderPoint(t, pos[0], pos[1]);
            }
            else if (pos.Count == 3)
            {
                return CalculBZOrderPoint(t, pos[0], pos[1], pos[2]);
            }
            else if (pos.Count == 4)
            {
                return CalculBZOrderPoint(t, pos[0], pos[1], pos[2], pos[3]);
            }
            else
            {
                return Vector3.zero;
            }
        }

        public static Vector3 CalculBZOrderPoint(float t, params object[] parm)
        {
            List<Vector3> lstV3 = new List<Vector3>();
            foreach (object item in parm)
            {
                lstV3.Add((Vector3)item);
            }
            return CalculBZOrderPoint(t, lstV3);
        }

        public static Vector3 CalculBZOrderPoint(float t, Vector3 p0, Vector3 p1)
        {
            return Vector3.Lerp(p0, p1, t);
        }

        public static Vector3 CalculBZOrderPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2)
        {
            return Qarth.BezierUtils.Calculate2OrderPoint(t, p0, p1, p2);
        }

        public static Vector3 CalculBZOrderPoint(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p4)
        {
            return Qarth.BezierUtils.Calculate3OrderPoint(t, p0, p1, p2, p4);
        }

        /// <summary>
        /// 切线方向
        /// </summary>
        public static Vector3 BezierTangent(float t, List<Vector3> pos)
        {
            if (pos.Count == 2)
            {
                return BezierTangent(t, pos[0], pos[1]);
            }
            else if (pos.Count == 3)
            {
                return BezierTangent(t, pos[0], pos[1], pos[2]);
            }
            else if (pos.Count == 4)
            {
                return BezierTangent(t, pos[0], pos[1], pos[2], pos[3]);
            }
            else
            {
                return Vector3.zero;
            }
        }

        public static Vector3 BezierTangent(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
        {
            float u = 1 - t;
            float uu = u * u;
            float tu = t * u;
            float tt = t * t;

            Vector3 P = p0 * 3 * uu * (-1.0f);
            P += p1 * 3 * (uu - 2 * tu);
            P += p2 * 3 * (2 * tu - tt);
            P += p3 * 3 * tt;
            //返回单位向量 
            return P.normalized;
        }

        //根据二阶贝塞尔曲线求的其切线
        public static Vector3 BezierTangent(float t, Vector3 p0, Vector3 p1, Vector3 p2)
        {
            Vector3 P = 2 * (t - 1) * p0;
            P += p1 * 2 * (1 - 2 * t);
            P += p2 * 2 * t;
            return P.normalized;
        }

        public static Vector3 BezierTangent(float t, Vector3 p0, Vector3 p1)
        {
            return (p1 - p0).normalized;
        }

        public static Vector3 BezierThirdPos(Vector3 p0, Vector3 p1, float heightRetio)
        {
            Vector3 midlepos = Vector3.Lerp(p0, p1, 0.5f);
            float dis = Vector3.Distance(p0, p1);
            Vector3 dir = p1 - p0;
            Vector3 dir2 = new Vector3(-dir.z, dir.x);
            Vector3 thirdPos = midlepos + dir2.normalized * dis * heightRetio;
            return thirdPos;
        }

    }
}