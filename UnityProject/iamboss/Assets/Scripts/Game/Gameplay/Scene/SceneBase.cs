using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;

namespace GameWish.Game
{
    public interface ISceneBase
    {
        void OnSceneCtrllerInited(ISceneCtrller ctrller);

        //场景加载
        void OnSceneLoaded(params object[] args);

        //场景重置
        void OnSceneReset();

        void OnSceneTick(float deltaTime);

        //关卡退出清理
        void OnSceneClean();
    }

}