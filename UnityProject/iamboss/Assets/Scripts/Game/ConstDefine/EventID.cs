﻿using UnityEngine;
using System.Collections;
using Qarth;

namespace GameWish.Game
{
    public enum EventID
    {
        //guide
        OnPanelClose,
        OnLanguageTableSwitchFinish,
        OnTaskAdd,
        ShowGuideLine,
        AddWord,
        UnLockPicBtn,
        OnGuidePropertyComplete,
        TaskStateChanged,

        //ui
        OnPropertyAdd,
        OnCardGet,
        OnCollectionGet,

        //scene
        OnSceneLoaded,

        //slot
        OnSlotTrigger,
        OnSlotReward,
        OnSlotRewardBall,

        //ballbrick
        OnPickablePicked,
        OnObstacleCtrl,
        OnPickableSupply,

        OnBallBonus,
        OnBallSupply,
        OnBallLowCount,

        OnBallSuppplyStartTick,
        OnBallSuppplyStopTick,

        //balloon
        OnBalloonFloatingBonus,
        OnBalloonLevelUp,
        OnBalloonBlast,
        OnBalloonBlastShowVal,
        OnBalloonNewStage,
        OnBalloonStageRefreshed,
        OnBalloonSetup,
    }
}
