﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Qarth;

namespace GameWish.Game
{
    public class Define
    {
        public const string FIRST_BONUS = "first_bonus";
        public const string SOUND_DEFAULT_SOUND = "Click";
        public const int LONG_SCREEN_OFFSET_TOP = 60;
        public const int LONG_SCREEN_OFFSET_BOTTOM = 15;

        #region GameEvent
        public const string START_GAME = "StartGaming";

        public const string BUZZ_STATE = "Buzz_State";
        public const string SHAKE_STATE = "Shake_State";
        #endregion

        //event
        public const string EVT_SHARE = "ShareGameLink";
        //SPRITE  NAME 
        public const string AD_PLACEMENT_REWARD = "MainReward";
        public const string AD_PLACEMENT_INTER = "MainInter";
        public const string AD_PLACEMENT_LEVEL_MIXVIEW = "LevelMixView";
        public const string AD_PLACEMENT_INFO_MIXVIEW = "InfoMixView";
        public const string AD_PLACEMENT_FAIL_MIXVIEW = "FailMixView";
        public const string AD_PLACEMENT_RED_MIXVIEW = "RedMixView";
        public const string AD_PLACEMENT_FAKE_RED_MIXVIEW = "FakeRedMixView";
        public const string AD_PLACEMENT_FAKE_LEVEL_MIXVIEW = "FakeLevelMixView";
        public const string AD_PLACEMENT_ACT_MIXVIEW = "ActMixView";

        public const string SIGN_DAY_KEY = "Gameplay_StartDate";


        public const string BALL_TAG = "Ball";
        public const string FLOOR_TAG = "Floor";
        public const string HITABLE_TAG = "Hitable";
        public const string PICKABLE_TAG = "Pickable";

        public const float SAFETY_CASH_COUNT = 99.98f;
        public const int SAFETY_CARD_COUNT = 999;
        public const int SlOT_CARD_COUNT = 50;


        public const int BALLOON_STAGE_LVL = 5;

        public const float BALL_SPEED = 10.0f;
        public const int BALL_SHOOTER_CAPACITY = 100;
        public const int PICKABLE_SLOT_COUNT = 9;
        public const int BALL_SUPPLY_LIMIT = 40;
        public const int BALL_SUPPLY_COUNT_LIMIT = 30;
        public const int BALL_SUPPLY_DURATION = 60;
        public const int BALL_TYPE_BONUS_DURATION = 100;
        public const float BALL_TYPE_BONUS_RATIO = 0.5f;
        public const int OBSTACLE_CTRL_DURATION = 60;
        public const float BALL_AUTO_SHOOT_DELAY = 0.2f;

        public const int SLOT_PRIZE_777_BALL_COUNT = 50;

        public const int BALLOON_BONUS_FIRST_CD = 120;
        public const int BALLOON_BONUS_NEXT_CD = 60;
        public const int BALLOON_BONUS_WANDER_DURATION = 10;



        public const int TOKEN_UPLOAD_PHASE = 100;
        public const int CASH_UPLOAD_PHASE = 10;
    }
}
