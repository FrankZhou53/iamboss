﻿using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Qarth;
using DG.Tweening;
using DoozyUI;
using Lean.Pool;

namespace GameWish.Game
{
    public class EffectControl : TMonoSingleton<EffectControl>
    {
        private ResLoader m_Loader;
        private Dictionary<string, GameObject> m_DictDynamicEffs = new Dictionary<string, GameObject>();


        private List<ParticleCtrller> m_LstPosEffs = new List<ParticleCtrller>();

        private GameObject m_EffRoot;

        public void InitEffectControl()
        {
            Log.i("InitEffectControl.");
            m_Loader = ResLoader.Allocate("EffectMgr");

            m_EffRoot = new GameObject("GameEffectRoot");
            m_EffRoot.transform.position = Vector3.one * 5000;
            m_EffRoot.transform.localEulerAngles = Vector3.zero;
            m_EffRoot.transform.localScale = Vector3.one;
            m_EffRoot.AddComponent<DontDestroyOnLoad>();
        }

        private void PlayParticleSystem(GameObject p, Vector3 position)
        {
            ParticleSystem ps = p.GetComponent<ParticleSystem>();
            ps.Clear();
            ps.transform.SetParent(m_EffRoot.transform);
            ps.transform.position = position;
            ps.Play();
        }
        #region pub_funcs

        public void PreLoadWithPool(string effName, int limit = 0)
        {
            try
            {
                if (string.IsNullOrEmpty(effName))
                    return;
                if (!m_DictDynamicEffs.ContainsKey(effName))
                {
                    GameObject eff = m_Loader.LoadSync(effName) as GameObject;
                    if (eff == null)
                    {
                        Log.e(effName + " not found");
                        return;
                    }
                    eff.SetActive(false);
                    eff.transform.parent = m_EffRoot.transform;
                    eff.transform.localPosition = Vector3.zero;
                    //eff.transform.localEulerAngles = Vector3.zero;

                    m_DictDynamicEffs.Add(effName, eff);

                    var poolObj = LeanPool.Spawn(eff, Vector3.zero, Quaternion.identity, m_EffRoot.transform, false);
                    if (poolObj != null)
                    {
                        if (limit > 0)
                        {
                            LeanPool.Links[poolObj].Capacity = limit;
                        }
                        LeanPool.Despawn(poolObj);

                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public void GenEffWithPool(string effName, int limit = 0)
        {
            try
            {
                if (string.IsNullOrEmpty(effName))
                    return;
                if (!m_DictDynamicEffs.ContainsKey(effName))
                {
                    GameObject eff = m_Loader.LoadSync(effName) as GameObject;
                    if (eff == null)
                    {
                        Log.e(effName + " not found");
                        return;
                    }
                    eff.SetActive(false);
                    eff.transform.parent = m_EffRoot.transform;
                    eff.transform.localPosition = Vector3.zero;
                    //eff.transform.localEulerAngles = Vector3.zero;

                    m_DictDynamicEffs.Add(effName, eff);

                    if (limit > 0)
                    {
                        var poolObj = LeanPool.Spawn(eff, Vector3.zero, Quaternion.identity, m_EffRoot.transform, false);
                        if (poolObj != null)
                        {
                            LeanPool.Links[poolObj].Capacity = limit;
                            LeanPool.Despawn(poolObj);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public GameObject GetEffPrefab(string effName)
        {
            GenEffWithPool(effName);

            if (m_DictDynamicEffs.ContainsKey(effName))
            {
                return m_DictDynamicEffs[effName];
            }
            return null;
        }

        public ParticleCtrller PlayPosEffect(string effName, Vector3 pos, bool isScale = false)
        {
            try
            {
                if (string.IsNullOrEmpty(effName))
                    return null;
                GenEffWithPool(effName);

                GameObject effect = LeanPool.Spawn(m_DictDynamicEffs[effName]);
                if (effect == null)
                    return null;
                var pc = effect.AddMissingComponent<ParticleCtrller>();
                effect.transform.SetParent(m_EffRoot.transform);
                effect.transform.position = pos;
                //effect.transform.localEulerAngles = Vector3.zero;

                if (isScale)
                {
                    effect.transform.localScale = Vector3.one;
                }

                m_LstPosEffs.Add(pc);
                return pc;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        #endregion
    }
}
