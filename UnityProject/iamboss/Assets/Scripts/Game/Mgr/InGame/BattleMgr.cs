﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using DG.Tweening;

namespace GameWish.Game
{
    public class BattleMgr : TMonoSingleton<BattleMgr>
    {
        [SerializeField]
        private Transform m_TrsRoleRoot;
        [SerializeField]
        private Transform m_TrsEnemyRoot;
        [SerializeField]
        private Transform m_TrsEffectRoot;
        [SerializeField]
        private Transform m_MainCamera;
        [SerializeField]
        private Transform m_RoleCamera;
        [SerializeField]
        private PlayerBase m_Player;
        [SerializeField]
        private GameObject m_ObjLoading;
        // Start is called before the first frame update
        public PlayerBase player
        {
            get
            {
                return m_Player;
            }
        }
        void Start()
        {
            m_ObjLoading.SetActive(false);
        }

        // Update is called once per frame
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.F1))
            {
                StartCamera();
            }
            if (Input.GetKeyDown(KeyCode.F5))
            {
                m_ObjLoading.SetActive(true);
                SceneLogicMgr.S.ChangeScene(SceneEnum.Gaming, false);
            }
        }

        public void CreateBloodB(Vector3 _pos)
        {
            GameObject go = Resources.Load("IamBoss/EnemyExplosion") as GameObject;
            //var go = GameplayMgr.S.MainLoader.LoadSync("EnemyExplosion");
            var blood = GameObject.Instantiate(go) as GameObject;
            blood.transform.parent = m_TrsEffectRoot;
            blood.transform.localPosition = new Vector3(_pos.x, 19.5f, _pos.z);
            blood.SetActive(true);
            CustomExtensions.CallWithDelay(this, () =>
            {
                Destroy(blood);
            }, 8);
            //m_Player.KillEnemy();
        }
        public void CreateBloodC(Vector3 _pos)
        {
            GameObject go = Resources.Load("IamBoss/EnemyExplosion") as GameObject;
            //var go = GameplayMgr.S.MainLoader.LoadSync("EnemyExplosion");
            var blood = GameObject.Instantiate(go) as GameObject;
            blood.transform.parent = m_TrsEffectRoot;
            blood.transform.localPosition = _pos;
            blood.SetActive(true);
            CustomExtensions.CallWithDelay(this, () =>
            {
                Destroy(blood);
            }, 8);
        }
        public void CreateEarthQuake(Vector3 _pos)
        {
            GameObject go = Resources.Load("IamBoss/EarthQuake") as GameObject;
            var quick = GameObject.Instantiate(go) as GameObject;
            quick.transform.parent = m_TrsEffectRoot;
            quick.transform.localPosition = _pos;
            quick.SetActive(true);
            CustomExtensions.CallWithDelay(this, () =>
            {
                Destroy(quick);
            }, 20);
        }
        public void CreateCannonShoot(Vector3 _pos)
        {
            GameObject go = Resources.Load("IamBoss/Bullet") as GameObject;
            var bullet = GameObject.Instantiate(go) as GameObject;
            bullet.transform.parent = m_TrsEffectRoot;
            bullet.transform.localPosition = _pos;
            bullet.SetActive(true);
        }
        public void CreateBulletDestory(Vector3 _pos)
        {
            GameObject go = Resources.Load("IamBoss/BulletDestorySmall") as GameObject;
            var bullet = GameObject.Instantiate(go) as GameObject;
            bullet.transform.parent = m_TrsEffectRoot;
            bullet.transform.localPosition = _pos;
            bullet.SetActive(true);
            CustomExtensions.CallWithDelay(this, () =>
            {
                Destroy(bullet);
            }, 20);
        }
        public void CreateBuildingDestory(Vector3 _pos, int _type)
        {
            GameObject go = null;
            switch (_type)
            {
                case 0:
                    {
                        go = Resources.Load("IamBoss/BuildingDestorySmall_new") as GameObject;
                        break;
                    }
                case 1:
                    {
                        go = Resources.Load("IamBoss/BuildingDestoryMiddle_new") as GameObject;
                        break;
                    }
                case 2:
                    {
                        go = Resources.Load("IamBoss/BuildingDestoryBig_new") as GameObject;
                        break;
                    }
            }
            if (go != null)
            {
                var building = GameObject.Instantiate(go) as GameObject;
                building.transform.parent = m_TrsEffectRoot;
                building.transform.position = _pos;
                building.SetActive(true);
                // CustomExtensions.CallWithDelay(this, () =>
                // {
                //     Destroy(building);
                // }, 20);
            }

        }
        private void StartCamera()
        {
            //小人跳舞

            //player出现
            CustomExtensions.CallWithDelay(this, () =>
            {
                m_Player.transform.SetLocalY(50);
                m_Player.gameObject.SetActive(true);
                m_Player.Jump();
                Sequence quence = DOTween.Sequence();
                quence.Append(m_Player.transform.DOMoveY(25f, 0.9f));
                quence.Append(m_Player.transform.DOMoveY(19.5f, 0.1f));
            }, 1);
            //镜头转换
            CustomExtensions.CallWithDelay(this, () =>
            {
                m_MainCamera.DOMove(new Vector3(34.35f, 53.45f, -112.65f), 3f);
                m_MainCamera.DORotate(new Vector3(45, 0, 0), 1.5f);
            }, 2.5f);
            CustomExtensions.CallWithDelay(this, () =>
            {
                m_RoleCamera.gameObject.SetActive(true);
                m_MainCamera.gameObject.SetActive(false);
            }, 5.5f);
        }
    }
}
