﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Qarth;
using UnityEngine;

namespace GameWish.Game
{
    public class GuideInfoHelper
    {
        public static string GetEditorSavePath(string prefabName)
        {
            string fileName = string.Format("guide_{0}.bin", prefabName);
            string path = string.Format("{0}/../GuideInfo", Application.dataPath);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            return string.Format("{0}/{1}", path, fileName);
        }

        public static string GetRuntimeSavePath(string prefabName)
        {
            string path = string.Format("guide_{0}.bin", prefabName);
            return string.Format("GuideInfo/{0}", path);
        }

        public static void SaveGuideInfo(string prefabName, GuideInfo info)
        {
            SerializeHelper.SerializeJson(GetEditorSavePath(prefabName), info, false);

            string filePath = GetRuntimeSavePath(prefabName);
            var path = (FilePath.streamingAssetsPath + filePath);
            path = path.Remove(path.LastIndexOf("/"));
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            SerializeHelper.SerializeJson(FilePath.streamingAssetsPath + filePath, info, false);
        }

        public static GuideInfo GetGuideInfo(string prefabName)
        {
            return SerializeHelper.DeserializeJsonRelativePath<GuideInfo>(GetRuntimeSavePath(prefabName), false);
        }

        public static void ExportAllGuideInfos()
        {
            string folder = string.Format("{0}/../GuideInfo", Application.dataPath);
            string target = Application.streamingAssetsPath + "/GuideInfo/";
            DirectoryInfo FolderInfo = new DirectoryInfo(target);
            if (FolderInfo.Exists)
            {
                FolderInfo.Delete(true);
            }
            FolderInfo.Create();

            FolderInfo = new DirectoryInfo(folder);
            FileInfo[] fileInfos = FolderInfo.GetFiles();
            for (int i = 0; i < fileInfos.Length; i++)
            {
                string name = fileInfos[i].Name;
                var infopath = Path.Combine(folder, name);

                using (FileStream fsRead = new FileStream(infopath, FileMode.Open))
                {
                    var info = SerializeHelper.DeserializeJson<GuideInfo>(fsRead, false);
                    var path = target + name;
                    SerializeHelper.SerializeJson(path, info, false);
                }
            }
            Debug.Log("Export Success");
        }

    }

    public class GuideInfo
    {
        public string Position1;
        public string Position2;
    }
}

