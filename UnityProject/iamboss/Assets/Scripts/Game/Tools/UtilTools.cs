﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Qarth;
using System;
namespace GameWish.Game
{

    public class UtilTools : TSingleton<UtilTools>
    {

        // Number随机数个数
        // minNum随机数下限
        // maxNum随机数上限
        public static int[] GetRandomArray(int Number, int minNum, int maxNum)
        {
            int j;
            int[] b = new int[Number];
            for (j = 0; j < Number; j++)
            {
                int i = RandomHelper.Range(minNum, maxNum);
                int num = 0;
                for (int k = 0; k < j; k++)
                {
                    if (b[k] == i)
                        num = num + 1;
                }
                if (num == 0)
                    b[j] = i;
                else
                    j = j - 1;
            }
            return b;
        }


        public static List<int> GetRandomArray(List<int> lst, int sum)
        {
            List<int> tempLst = new List<int>();
            lst.ForEach(i => tempLst.Add(i));
            if (tempLst.Count < sum)
            {
                Log.e("tempLst.Count = " + tempLst.Count + "  sum = " + sum);
                return null;
            }
            else
            {
                List<int> temp = new List<int>();
                for (int i = 0; i < sum; i++)
                {
                    int index = RandomHelper.Range(0, tempLst.Count);
                    temp.Add(tempLst[index]);
                    tempLst.RemoveAt(index);
                }
                return temp;
            }
        }

        public static List<int> GetDisturbArray(List<int> lst)
        {
            List<int> tempLst = new List<int>();
            List<int> newLst = new List<int>();
            lst.ForEach(i => tempLst.Add(i));
            for (int i = tempLst.Count - 1; i >= 0; i--)
            {
                int randomIndex = RandomHelper.Range(0, tempLst.Count);
                newLst.Add(tempLst[randomIndex]);
                tempLst.RemoveAt(randomIndex);
            }
            return newLst;
        }

        public static string[] GetArrStringSplit(string str)
        {
            string[] _str = str.Split('|');
            return _str;
        }

        public static string GetYesterdayMonthStrDay()
        {
            string str1 = "";
            // string ste = DateTime.Now.ToString("MMMM");//, new System.Globalization.CultureInfo("en-us")); // 月份只要洋文的前三个字母
            DateTime dateTime = DateTime.Now.AddDays(-1);
            str1 = dateTime.ToString("MMMM");
            return string.Concat(str1, " ", dateTime.Day, "th");
        }

    }

}