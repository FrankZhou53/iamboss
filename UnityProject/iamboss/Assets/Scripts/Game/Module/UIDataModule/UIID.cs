﻿using UnityEngine;
using System.Collections;
using Qarth;

namespace GameWish.Game
{
    public enum UIID
    {
        LogoPanel = 0,
        //测试工具界面
        ToolsPanel,
        SplashPanel,
        GuideWordsPanel,
        LoadingPanel,
        WorldUIPanel,
        GamingPanel,
        BalloonPanel,

        SlotPanel,
        SlotAgainPanel,
        SupplyBallPanel,
        DirectHitPanel,
        RemoveBlockPanel,

        SettingPanel,
        MainPanel,
        RulePanel,

        SignInPanel,
        OfflinePanel,

        RedpackGetPanel,

        LevelFailedPanel,
        LevelPassPanel,

        TopBarPanel,
        //玩家详情界面
        PlayerInfoPanel,
        //任务界面
        TaskPanel,
        GetRewardPanel,
        //引导点击界面
        GuideButtonClickPanel,
        //引导提示字界面
        MyGuideWordsPanel,
        SimpleGuideHandPanel,


        RedeemWithdrawPanel,
        RedeemSorryPanel,
        RedeemRecordPanel,
        RedeemPanel,
        RedeemTipsPanel,

        CollectionPanel,
    }
}
