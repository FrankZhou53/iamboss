using System;
using System.Collections;
using System.Collections.Generic;
using Qarth;
using UnityEngine;

namespace GameWish.Game
{
    public class GuidePropertyCompleteTrigger : ITrigger
    {
        private bool m_IsClose;
        protected Action<bool, ITrigger> m_Listener;

        public bool isReady
        {
            get
            {
                return m_IsClose;
            }
        }
        public void SetParam(object[] param)
        {

        }

        public void Start(Action<bool, ITrigger> l)
        {
            m_IsClose = false;
            m_Listener = l;
            EventSystem.S.Register(EventID.OnGuidePropertyComplete, OnEventListerner);
        }

        public void Stop()
        {
            m_Listener = null;
            EventSystem.S.UnRegister(EventID.OnGuidePropertyComplete, OnEventListerner);
        }

        private void OnEventListerner(int key, params object[] args)
        {
            if (m_Listener == null)
            {
                return;
            }

            m_IsClose = true;

            m_Listener(true, this);
            m_Listener = null;
        }
    }
}

