﻿using System;
using System.Collections;
using System.Collections.Generic;
using Qarth;
using UnityEngine;

namespace GameWish.Game
{
	public class ShowGuideLineCommand : AbstractGuideCommand
    {
        private bool m_NeedClose = true;
        private string m_Params;

        public override void SetParam(object[] pv)
        {
            if (pv.Length == 0)
            {
                Log.w("ShowGuideLineCommand Init With Invalid Param.");
                return;
            }

            try
            {
                m_NeedClose = Helper.String2Bool(pv[0].ToString());
                if (pv.Length > 0)
                {
                    m_Params = pv[0].ToString();
                }
            }
            catch (Exception e)
            {
                Log.e(e);
            }
        }

        protected override void OnStart()
        {
            EventSystem.S.Send(EventID.ShowGuideLine, true, m_Params);
            EventSystem.S.Register(EventID.AddWord, OnFinishStep);
        }

        protected override void OnFinish(bool forceClean)
        {
        }

        void OnFinishStep(int key, params object[] para)
        {
            if (para != null && para.Length > 0) 
            {
                if (para[0].ToString() == m_Params)
                {
                    EventSystem.S.Send(EventID.ShowGuideLine, false);
                    EventSystem.S.UnRegister(EventID.AddWord, OnFinishStep);
                    FinishStep();
                }
            }
        }
    }
}

