//Auto Generate Don't Edit it
using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using Qarth;

namespace GameWish.Game
{
    public partial class TDEmptyTable
    {
        
       
        private EInt m_ID = 0;   
        private EInt m_Times = 0;   
        private EInt m_MinLevel = 0;   
        private EInt m_MaxLevel = 0;   
        private string m_Reward;   
        private string m_Desc;   
        private string m_Icon;  
        
        //private Dictionary<string, TDUniversally.FieldData> m_DataCacheNoGenerate = new Dictionary<string, TDUniversally.FieldData>();
      
        /// <summary>
        /// 签到奖励ID
        /// </summary>
        public  int  iD {get { return m_ID; } }
       
        /// <summary>
        /// 次数
        /// </summary>
        public  int  times {get { return m_Times; } }
       
        /// <summary>
        /// 关卡下限
        /// </summary>
        public  int  minLevel {get { return m_MinLevel; } }
       
        /// <summary>
        /// 关卡上限
        /// </summary>
        public  int  maxLevel {get { return m_MaxLevel; } }
       
        /// <summary>
        /// 奖励
        /// </summary>
        public  string  reward {get { return m_Reward; } }
       
        /// <summary>
        /// 描述
        /// </summary>
        public  string  desc {get { return m_Desc; } }
       
        /// <summary>
        /// 图标
        /// </summary>
        public  string  icon {get { return m_Icon; } }
       

        public void ReadRow(DataStreamReader dataR, int[] filedIndex)
        {
          //var schemeNames = dataR.GetSchemeName();
          int col = 0;
          while(true)
          {
            col = dataR.MoreFieldOnRow();
            if (col == -1)
            {
              break;
            }
            switch (filedIndex[col])
            { 
            
                case 0:
                    m_ID = dataR.ReadInt();
                    break;
                case 1:
                    m_Times = dataR.ReadInt();
                    break;
                case 2:
                    m_MinLevel = dataR.ReadInt();
                    break;
                case 3:
                    m_MaxLevel = dataR.ReadInt();
                    break;
                case 4:
                    m_Reward = dataR.ReadString();
                    break;
                case 5:
                    m_Desc = dataR.ReadString();
                    break;
                case 6:
                    m_Icon = dataR.ReadString();
                    break;
                default:
                    //TableHelper.CacheNewField(dataR, schemeNames[col], m_DataCacheNoGenerate);
                    break;
            }
          }

        }
        
        public static Dictionary<string, int> GetFieldHeadIndex()
        {
          Dictionary<string, int> ret = new Dictionary<string, int>(7);
          
          ret.Add("ID", 0);
          ret.Add("Times", 1);
          ret.Add("MinLevel", 2);
          ret.Add("MaxLevel", 3);
          ret.Add("Reward", 4);
          ret.Add("Desc", 5);
          ret.Add("Icon", 6);
          return ret;
        }
    } 
}//namespace LR